﻿using azNewsImagesUploader.Helpers;
using azNewsImagesUploader.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace azNewsImagesUploader.Controllers
{
    [Route("[controller]")]
    public class UploadController : Controller
    {
        // make sure that appsettings.json is filled with the necessary details of the azure storage
        private readonly AzureStorageConfig storageConfig = null;

        public UploadController(IOptions<AzureStorageConfig> config)
        {
            storageConfig = config.Value;
        }

        // POST /upload
        [HttpPost]
        public async Task<IActionResult> Upload(ICollection<IFormFile> files)
        {
            bool isUploaded = false;
            try
            {
                if (files.Count == 0)
                    return BadRequest("No files received from the upload");

                if (storageConfig.AccountKey == string.Empty || storageConfig.AccountName == string.Empty)
                    return BadRequest("sorry, can't retrieve your azure storage details from appsettings.js, make sure that you add azure storage details there");

                if (storageConfig.ImageContainer == string.Empty)
                    return BadRequest("Please provide a name for your image container in the azure blob storage");
                Dictionary<int, string> imageNames = new Dictionary<int, string>();
                int i = 1;
                foreach (var formFile in files)
                {                    
                    if (StorageHelper.IsImage(formFile))
                    {
                        if (formFile.Length > 0)
                        {                            
                            using (Stream stream = formFile.OpenReadStream())
                            {
                                var url = await StorageHelper.UploadFileToStorage(stream, formFile.FileName, storageConfig);
                                if (!string.IsNullOrEmpty(url))
                                {
                                    isUploaded = true;
                                }
                                if (isUploaded)
                                {
                                    imageNames.Add(i, url);
                                    i++;
                                }
                            }
                        }
                    }
                    else
                    {
                        return new UnsupportedMediaTypeResult();
                    }
                }
                if (imageNames.Count() >= 1)
                {
                    return Ok(JsonConvert.SerializeObject(imageNames));
                }
                else
                    return BadRequest("Look like the image couldnt upload to the storage");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}